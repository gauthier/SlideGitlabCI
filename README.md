Support de cours Gitlab-CI
====================

Diapos pour le cours sur Gitlab-CI.

Utilisation
-----------

    $ npm install

et ensuite ouvrir le fichier `index.html` avec un navigateur web avec
le support de JavaScript activé.

Visualisation en ligne
----------------------

https://gauthier.frama.io/SlideGitlabCI
